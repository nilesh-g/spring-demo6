package com.sunbeaminfo.sh;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class Sp06Main {
	public static String postsUrl = "https://jsonplaceholder.typicode.com/posts";
	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx;
		ctx = new ClassPathXmlApplicationContext("/beans.xml");
		
		RestTemplate restTemplate = new RestTemplate();
		
//		Post post = (Post) restTemplate.getForObject(postsUrl + "/1", Post.class);
//		System.out.println(post);
		
//		ResponseEntity<Post[]> postsResp = restTemplate.getForEntity(postsUrl, Post[].class);
//		if(postsResp.getStatusCode() == HttpStatus.OK) {
//			 Post[] posts = postsResp.getBody();
//			 for (Post post : posts) 
//				 System.out.println(post);
//		}
		
		ctx.close();
	}
}
